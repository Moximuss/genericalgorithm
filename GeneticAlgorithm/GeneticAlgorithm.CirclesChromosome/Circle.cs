﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneticAlgorithm.RectanglesGenotype;

namespace GeneticAlgorithm.CirclesChromosome
{
    public sealed class Circle : XYZPoint
    {
        public double Radius { get; set ; }

    }
}
