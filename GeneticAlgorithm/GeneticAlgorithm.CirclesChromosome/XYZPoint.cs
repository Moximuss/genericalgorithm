﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.CirclesChromosome
{
    public class XYZPoint
    {
        public double X { get; set; }
        public double Y { get; set; }
        public bool IsRelative2Circles { get; set; }
    }
}
