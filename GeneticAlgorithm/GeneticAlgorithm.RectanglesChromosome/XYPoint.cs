﻿namespace GeneticAlgorithm.RectanglesGenotype
{
    public class XYPoint
    {
        public double X { get; set; }
        public double Y { get; set; }
        public bool IsRelative2Circles { get; set; }
    }
}