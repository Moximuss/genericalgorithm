﻿namespace GeneticAlgorithm.RectanglesGenotype
{
    public class Rectangle
    {
        public XYPoint Position { get; set; }
        public double Height { get; set; }
        public double Width { get; set; }
    }
}
