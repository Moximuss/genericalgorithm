﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using GeneticAlgorithm.CirclesChromosome.Services;
using RectChromosome = GeneticAlgorithm.RectanglesGenotype.RectanglesChromosome;

namespace GeneticAlgorithm.Presentation.Pages
{
    public partial class PlacedRectangles : Page
    {
        private Point clickPosition = new Point(0, 0);
        public double Scale = 1;

        public PlacedRectangles()
        {
            InitializeComponent();
            AppShared.DrawRectanglesOnCanvas += PlaceRectanglesAsync;
            if (!(AppShared.Rectangles is null))
            {
                PlaceRectanglesAsync();
            }
        }

        private void DrawMainRect()
        {
            DrawRectangle(RectChromosome.TopBorder, AppShared.RecatangleResult?.Fitness() ?? 0, 0, 0, Brushes.Red);
        }

        public void PlaceRectanglesAsync()
        {
            Task.Run(() =>
            {
                try
                {
                    if (RectChromosome.Source is null)
                    {
                        return;
                    }
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        placingResultCanvas.Children.Clear();

                        for (int i = 0; i < AppShared.RecatangleResult.Genotype.Length; i++)
                        {
                            var rect = RectChromosome.Source[AppShared.RecatangleResult.Genotype[i]];
                            DrawRectangle(rect.Height, rect.Width, AppShared.RecatangleResult.Positions[i].X, AppShared.RecatangleResult.Positions[i].Y);
                        }
                        DrawMainRect();
                    });
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK);
                }
            });
        }

        private void DrawRectangle(double height, double width, double x, double y, Brush brush = null)
        {
            if (brush == null)
            {
                brush = Brushes.Black;
            }

            var shape = new Rectangle() { Height = height, Width = width, Stroke = brush };
            Canvas.SetLeft(shape, x);
            Canvas.SetBottom(shape, y);

            placingResultCanvas.Children.Add(shape);
        }

        private void PlacingResultCanvas_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            var mousePos = e.GetPosition(placingResultCanvas);

            mousePos.Y = placingResultCanvas.ActualHeight - mousePos.Y;

            foreach (Shape element in placingResultCanvas.Children)
            {
                var left = Canvas.GetLeft(element);
                var bottom = Canvas.GetBottom(element);

                double dragX = (left - mousePos.X) * 0.05 * Math.Sign(e.Delta);
                double dragY = (bottom - mousePos.Y) * 0.05 * Math.Sign(e.Delta);

                Canvas.SetLeft(element, left + dragX);
                Canvas.SetBottom(element, bottom + dragY);

                element.Height += element.Height * 0.05 * Math.Sign(e.Delta);
                element.Width += element.Width * 0.05 * Math.Sign(e.Delta);
            }
        }
        private void PlacingResultCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var mousePos = e.GetPosition(placingResultCanvas);

                mousePos.Y = placingResultCanvas.ActualHeight - mousePos.Y;

                foreach (Shape element in placingResultCanvas.Children)
                {
                    var left = Canvas.GetLeft(element);
                    var bottom = Canvas.GetBottom(element);
                    Canvas.SetLeft(element, left + (mousePos.X - clickPosition.X));
                    Canvas.SetBottom(element, bottom + (mousePos.Y - clickPosition.Y));
                }

                clickPosition = mousePos;
            }
        }
        private void PlacingResultCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            clickPosition = e.GetPosition(placingResultCanvas);
            clickPosition.Y = placingResultCanvas.ActualHeight - clickPosition.Y;
        }

        int index = 0;
        RectChromosome chromosomeForStepPlacing = null;

        private void Next_Click(object sender, RoutedEventArgs e)
        {
            if (RectChromosome.Source is null)
            {
                MessageBox.Show("Set source.", "Err");
                return;

                if (chromosomeForStepPlacing is null)
                {
                    index = 0;
                    chromosomeForStepPlacing = AppShared.RecatangleResult is null
                        ? new RectChromosome()
                        : AppShared.RecatangleResult;
                    chromosomeForStepPlacing.Fitness();
                }
                if (index == RectChromosome.Source.Count)
                {
                    index = 0;
                    MessageBox.Show("All rects.", "");
                    chromosomeForStepPlacing = null;
                    return;
                }
                if (index == 0)
                {
                    placingResultCanvas.Children.Clear();
                }
                int ind = chromosomeForStepPlacing.Genotype[index];
                var shape = new Rectangle() { Height = RectChromosome.Source[ind].Height, Width = RectChromosome.Source[ind].Width, Stroke = Brushes.Black };
                Canvas.SetLeft(shape, chromosomeForStepPlacing.Positions[index].X);
                Canvas.SetBottom(shape, chromosomeForStepPlacing.Positions[index].Y);
                placingResultCanvas.Children.Add(shape);
                index++;
            }
        }

        private void SetSequence_Click(object sender, RoutedEventArgs e)
        {
            if (RectChromosome.Source == null)
            {
                return;
            }
            if (AppShared.RecatangleResult == null)
            {
                AppShared.RecatangleResult = new RectChromosome();
            }
            List<int> array = sequenceTextBox.Text.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(s => int.Parse(s)).ToList();
            if (array.Count != RectChromosome.Source.Count)
            {
                return;
            }

            AppShared.RecatangleResult.Genotype = array.ToArray();
            AppShared.RecatangleResult.ResetFitness();
            AppShared.RecatangleResult.Fitness();
            PlaceRectanglesAsync();
        }
    }
}
