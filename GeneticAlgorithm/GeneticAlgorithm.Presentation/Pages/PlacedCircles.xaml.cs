﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GeneticAlgorithm.CirclesChromosome;
using GeneticAlgorithm.Presentation.Services;
using RectChromosome = GeneticAlgorithm.RectanglesGenotype.RectanglesChromosome;

namespace GeneticAlgorithm.Presentation.Pages
{
    /// <summary>
    /// Interaction logic for PlacedCircles.xaml
    /// </summary>
    public partial class PlacedCircles : Page
    {
        public double Scale = 1;
        public PlacedCircles()
        {
            InitializeComponent();
            CirclePlacingService.drawCircle += DrawCircle;
            CirclePlacingService.writeMessage += Message;
            CirclePlacingService.replaceEllipse += RemoveEllipse;
            RunAsync();

        }
        public void PlaceRectanglesAsync()
        {
            Task.Run(() =>
            {
                try
                {
                    var circles = CirclesChromosome.CirclesChromosome.Resource;

                    if (CirclesChromosome.CirclesChromosome.Resource == null)
                    {
                        circles = CirclesDataService.GetRandomCircles(100, 10, 50);
                    }
                    CirclePlacingService.CirclesPlacing(circles, 300, 600);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK);
                }
            });
        }


        private void Message(string message)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                messageInfo.Content = message;
            });
        }

        private void RemoveEllipse(Ellipse ellipse)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                placingCirclesCanvas.Children.Remove(ellipse);
            });
        }

        private void DrawMainRect()
        {
            DrawRectangle(RectChromosome.TopBorder, AppShared.RecatangleResult?.Fitness() ?? 0, 0, 0, Brushes.Red);
        }
       
        private void DrawRectangle(double height, double width, double x, double y, Brush brush = null)
        {
            if (brush == null)
            {
                brush = Brushes.Black;
            }
            var shape = new Rectangle() { Height = height, Width = width, Stroke = brush };
            Canvas.SetLeft(shape, x);
            Canvas.SetBottom(shape, y);

            placingCirclesCanvas.Children.Add(shape);
        }
        private Ellipse DrawCircle(Circle circle, Brush brush = null)
        {
            Ellipse eliplse = null;
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (brush == null)
                    brush = Brushes.Black;
                eliplse = new Ellipse();
                eliplse.Stroke = brush;
                eliplse.Height = circle.Radius * 2.0 * Scale;
                eliplse.Width = eliplse.Height;
                Canvas.SetLeft(eliplse, (circle.X - circle.Radius) * Scale);
                Canvas.SetBottom(eliplse, (circle.Y - circle.Radius) * Scale);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    placingCirclesCanvas.Children.Add(eliplse);
                });
            });

            return eliplse;
        }

        private async void RunAsync()
        {
            var circles = CirclesChromosome.CirclesChromosome.Resource;
            if (CirclesChromosome.CirclesChromosome.Resource == null)
            {
                circles = CirclesDataService.GetRandomCircles(100, 10, 50);
            }

            await CirclePlacingService.CirclesPlacing(circles, 300, 600);
        }
    }
}
